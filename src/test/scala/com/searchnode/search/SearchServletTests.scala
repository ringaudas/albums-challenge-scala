package com.searchnode.search

import org.scalatra.test.scalatest._

class SearchServletTests extends ScalatraFunSuite {

  addServlet(classOf[SearchServlet], "/*")

  test("GET / on SearchServlet should return status 200") {
    get("/") {
      status should equal (200)
    }
  }

}
