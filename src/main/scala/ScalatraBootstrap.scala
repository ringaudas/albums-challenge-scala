import com.searchnode.search._
import org.scalatra._

import javax.servlet.ServletContext
import com.searchnode.search.SearchController.{downloadFeed}

class ScalatraBootstrap extends LifeCycle {
  override def init(context: ServletContext) {
    implicit val entries = downloadFeed()
    println("Loaded index")
    context.mount(new SearchServlet, "/*")
  }
}
