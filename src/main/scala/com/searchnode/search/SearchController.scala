package com.searchnode.search

import org.json4s._
import org.json4s.jackson.JsonMethods._

import scala.io.Source.fromURL
import scala.util.{Failure, Success, Try, Using}

object SearchController {
  def downloadFeed(): Option[Vector[Entry]] = {
    val url = "https://itunes.apple.com/us/rss/topalbums/limit=200/json"
    implicit val formats: Formats = DefaultFormats

    Using(fromURL(url))(_.mkString) match {
      case Success(v) =>
        parse(v).getAs(Entry.reader)
      case Failure(_) =>
        println("Failed to fetch feed")
        None
    }
  }

  def search(entries: Vector[Entry], searchQuery: String, filters: Map[String, Vector[String]]): Results = {
    println(f"Searching by $searchQuery")
    println(filters)
    Results(
      items = entries,
      count = entries.length,
      facets = Map(
        "price" -> Vector(
          Facet("5 - 10", 25),
          Facet("10 - 15", 2),
          Facet("20 - 25", 54),
        ),
        "year" -> Vector(
          Facet("2015", 25),
          Facet("2017", 2),
          Facet("2018", 54),
        )
      ),
      searchQuery
    )
  }
}
