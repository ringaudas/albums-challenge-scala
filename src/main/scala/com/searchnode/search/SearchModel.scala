package com.searchnode.search

import org.json4s.{DefaultFormats, Formats, JObject, JValue, Reader}

case class Entry(title: String, link: String, price: BigDecimal, releaseDate: String, image: Option[String])

object Entry {
  implicit val formats: Formats = DefaultFormats
  val reader: Reader[Vector[Entry]] = (value: JValue) => {
    (value \ "feed" \ "entry").extract[Vector[JObject]].map(v => {
      Entry(
        title = (v \ "title" \ "label").extract[String],
        link = (v \ "link" \ "attributes" \ "href").extract[String],
        price = BigDecimal((v \ "im:price" \ "attributes" \ "amount").extract[String]),
        releaseDate = (v \ "im:releaseDate" \ "label").extract[String],
        image = ((v \ "im:image").extract[Vector[JObject]].headOption).map(v => (v \ "label").extract[String])
      )
    })
  }
}

case class Error(error: String)

case class Results(items: Vector[Entry], count: Int, facets: Map[String, Vector[Facet]], searchQuery: String)

case class Facet(value: String, count: Int)
