package com.searchnode.search

import com.searchnode.search.SearchController._
import org.scalatra._
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._

class SearchServlet(implicit entries: Option[Vector[Entry]]) extends ScalatraServlet with JacksonJsonSupport {

  protected implicit val jsonFormats: Formats = DefaultFormats

  get("/") {
    contentType="text/html"

    views.html.search.render()
  }
  get("/api") {
    contentType = formats("json")

    entries match {
      case Some(v) =>
        search(v, params.getOrElse("query", ""), Map(
          "year" -> multiParams.getOrElse("year", Nil).toVector,
          "price" -> multiParams.getOrElse("price", Nil).toVector,
        ))
      case None =>
        Error("Failed to load index")
    }
  }
}
