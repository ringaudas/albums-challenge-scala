import { renderSidebar, renderContent, bindFacetChange } from './rendering.js'


export async function executeSearch(searchQuery, filters) {
    console.log(`Searching for "${searchQuery}" with filters`, filters)

    const params = new URLSearchParams({
        query: searchQuery
    })
    Object.entries(filters).forEach(([k, vs]) => {
        vs.forEach(v => {
            params.append(k, v)
        })
    })

    const { facets, items } = await fetch(`/api?${params.toString()}`).then(v => v.json())

    console.log('Got response', {facets, items})

    renderSidebar(facets, filters)
    renderContent(items)
    bindFacetChange(newFilters => {
        executeSearch(searchQuery, newFilters)
    })

}
