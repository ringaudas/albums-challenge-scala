val ScalatraVersion = "2.7.1"

ThisBuild / scalaVersion := "2.13.3"
ThisBuild / organization := "com.searchnode"

lazy val hello = (project in file("."))
  .settings(
    name := "search",
    version := "1.0.0",
    libraryDependencies ++= Seq(
      "org.scalatra" %% "scalatra" % ScalatraVersion,
      "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
      "org.scalatra" %% "scalatra-json" % ScalatraVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime",
      "org.eclipse.jetty" % "jetty-webapp" % "9.4.35.v20201120" % "container",
      "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
      "org.json4s" %% "json4s-jackson" % "3.6.11"
    ),
  )

enablePlugins(SbtTwirl)
enablePlugins(JettyPlugin)
